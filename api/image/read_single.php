<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/Image.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate blog post object
  $image = new Image($db);

  // Get ID
  $image->id = isset($_GET['id']) ? $_GET['id'] : die();

  // Get post
  $image->read_single();

  // Create array
  $img_arr = array(
    'id' => $id,
    'name' => $name,
    'id' => $id,
    'artifact_name' => $artifact_name,
    'media_name' => $media_name,
    'file_name' => $file_name,
    'titlos' => $titlos,
    'gegonos' => $gegonos,
    'eidiki_perigrafi' => $eidiki_perigrafi,
    'analytiki_tekmiriosi' => $analytiki_tekmiriosi,
    'proswpa' => $proswpa,
    'fylo' => $fylo,
    'endymasia' => $endymasia,
    'limmata' => $limmata,
    'geniki_perigrafi' => $geniki_perigrafi,
    'topos' => $topos,
    'proeleysi' => $proeleysi
  );

  // Make JSON
  print_r(json_encode($img_arr));