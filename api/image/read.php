<?php 
  // Headers
  header('Access-Control-Allow-Origin: *');
  header('Content-Type: application/json');

  include_once '../../config/Database.php';
  include_once '../../models/Image.php';

  // Instantiate DB & connect
  $database = new Database();
  $db = $database->connect();

  // Instantiate image object
  $image = new Image($db);

  // Image read query
  $result = $image->read();
  
  // Get row count
  $num = $result->rowCount();

  // Check if any images
  if($num > 0) {
        // Cat array
        $img_arr = array();
        $img_arr['data'] = array();

        while($row = $result->fetch(PDO::FETCH_ASSOC)) {
          extract($row);

          $img_item = array(
            'id' => $id,
            'artifact_name' => $artifact_name,
            'media_name' => $media_name,
            'file_name' => $file_name,
            'titlos' => $titlos,
            'gegonos' => $gegonos,
            'eidiki_perigrafi' => $eidiki_perigrafi,
            'analytiki_tekmiriosi' => $analytiki_tekmiriosi,
            'proswpa' => $proswpa,
            'fylo' => $fylo,
            'endymasia' => $endymasia,
            'limmata' => $limmata,
            'geniki_perigrafi' => $geniki_perigrafi,
            'topos' => $topos,
            'proeleysi' => $proeleysi
          );

          // Push to "data"
          array_push($img_arr['data'], $img_item);
        }

        // Turn to JSON & output
        echo json_encode($img_arr);

  } else {
        // No Categories
        echo json_encode(
          array('message' => 'No Images Found')
        );
  }
