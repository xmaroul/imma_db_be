<?php
  class Image {
    // DB Stuff
    private $conn;
    private $table = 'images';

    // Properties
    public $id;
    public $artifact_name;
    public $media_name;
    public $file_name;
    public $titlos;
    public $gegonos;
    public $eidiki_perigrafi;
    public $analytiki_tekmiriosi;
    public $proswpa;
    public $fylo;
    public $endymasia;
    public $limmata;
    public $geniki_perigrafi;
    public $topos;
    public $proeleysi;
    private $all_fields = ['id',
                            'artifact_name',
                            'media_name',
                            'file_name',
                            'titlos',
                            'gegonos',
                            'eidiki_perigrafi',
                            'analytiki_tekmiriosi',
                            'proswpa',
                            'fylo',
                            'endymasia',
                            'limmata',
                            'geniki_perigrafi',
                            'topos',
                            'proeleysi'];

    // Constructor with DB
    public function __construct($db) {
      $this->conn = $db;
    }

    // Get categories
    public function read() {
      
      // Create query
      $query = 'SELECT '.implode(',',$this->all_fields).'         
      FROM
        ' . $this->table;
        $query .= ' WHERE file_name NOT LIKE "%NIKOLTSIOS%" ';
      if($_GET['search']){
        $query .= ' AND (analytiki_tekmiriosi LIKE "%'.$_GET['search'].'%" ';
        $query .= ' OR titlos LIKE "%'.$_GET['search'].'%" ';
        $query .= ' OR gegonos LIKE "%'.$_GET['search'].'%" ';
        $query .= ' OR eidiki_perigrafi LIKE "%'.$_GET['search'].'%" ';
        $query .= ' OR proswpa LIKE "%'.$_GET['search'].'%" ';
        $query .= ' OR endymasia LIKE "%'.$_GET['search'].'%" ';
        $query .= ' OR geniki_perigrafi LIKE "%'.$_GET['search'].'%" ';
        $query .= ' OR topos LIKE "%'.$_GET['search'].'%" ';
        $query .= ' OR proeleysi LIKE "%'.$_GET['search'].'%" )';
      }      
      $query .= ' ORDER BY
        id DESC';
      if(isset($_GET['limit'])){
        $query .= ' LIMIT '.$_GET['limit'];
      }
      // Prepare statement
      $stmt = $this->conn->prepare($query);

      // Execute query
      $stmt->execute();

      return $stmt;
    }

    // Get Single Image
  public function read_single(){
    // Create query
    $query = 'SELECT '.implode(',',$this->all_fields).' 
        FROM
          ' . $this->table . '
      WHERE id = ?
      LIMIT 0,1';

      //Prepare statement
      $stmt = $this->conn->prepare($query);

      // Bind ID
      $stmt->bindParam(1, $this->id);

      // Execute query
      $stmt->execute();

      $row = $stmt->fetch(PDO::FETCH_ASSOC);

      // set properties
      foreach ($this->all_fields as $field){
        $this->$field = $row[$field];
      }
      
  }

  // // Create Image
  // public function create() {
  //   // Create Query
  //   $query = 'INSERT INTO ' .
  //     $this->table . '
  //   SET
  //     name = :name';

  //   // Prepare Statement
  //   $stmt = $this->conn->prepare($query);

  //   // Clean data
  //   $this->name = htmlspecialchars(strip_tags($this->name));

  //   // Bind data
  //   $stmt-> bindParam(':name', $this->name);

  //   // Execute query
  //   if($stmt->execute()) {
  //     return true;
  //   }

  //   // Print error if something goes wrong
  //   printf("Error: $s.\n", $stmt->error);

  //   return false;
  // }

  // // Update Image
  // public function update() {
  //   // Create Query
  //   $query = 'UPDATE ' .
  //     $this->table . '
  //   SET
  //     name = :name
  //     WHERE
  //     id = :id';

  //   // Prepare Statement
  //   $stmt = $this->conn->prepare($query);

  //   // Clean data
  //   $this->name = htmlspecialchars(strip_tags($this->name));
  //   $this->id = htmlspecialchars(strip_tags($this->id));

  //   // Bind data
  //   $stmt-> bindParam(':name', $this->name);
  //   $stmt-> bindParam(':id', $this->id);

  //   // Execute query
  //   if($stmt->execute()) {
  //     return true;
  //   }

  //   // Print error if something goes wrong
  //   printf("Error: $s.\n", $stmt->error);

  //   return false;
  // }

  // // Delete Image
  // public function delete() {
  //   // Create query
  //   $query = 'DELETE FROM ' . $this->table . ' WHERE id = :id';

  //   // Prepare Statement
  //   $stmt = $this->conn->prepare($query);

  //   // clean data
  //   $this->id = htmlspecialchars(strip_tags($this->id));

  //   // Bind Data
  //   $stmt-> bindParam(':id', $this->id);

  //   // Execute query
  //   if($stmt->execute()) {
  //     return true;
  //   }

  //   // Print error if something goes wrong
  //   printf("Error: $s.\n", $stmt->error);

  //   return false;
  // }
}
